<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />
<!-- Required meta tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
<link rel="stylesheet" href="style.css">

<title>Calculadora IMC</title>
</head>

<body>
	<%!		
		int resultImcTable;
	
		public boolean isValidForm(HttpServletRequest r) {
			String sWeight = r.getParameter("peso");
			String sHeight = r.getParameter("altura");
			
			if (sWeight == null || sHeight == null || sWeight.isEmpty() || sHeight.isEmpty()) {
				return false;
			}
		
		    float weight = Float.parseFloat(sWeight);
		    float height = Float.parseFloat(sHeight);
	
		    // Valida os campos
		    if (height <= 0 || height >= 3 || weight <= 0 || weight >= 300) {
		        return false;
		    }
		    return true;
		}
	
		public float calculateIMC(HttpServletRequest r) {			
		    float weight = Float.parseFloat(r.getParameter("peso"));
		    float height = Float.parseFloat(r.getParameter("altura"));
		    return weight / (height * height);
		}
		
		public String getIMCResult(HttpServletRequest r) {
		    float weight = Float.parseFloat(r.getParameter("peso"));
		    float height = Float.parseFloat(r.getParameter("altura"));
			float imc = calculateIMC(r);
			String message = (imc >= 18.5 && imc < 24.9) ? "Parabéns! Seu IMC é de " : "Sinto muito, mas seu IMC é de ";
		    message += String.format("%.01f", imc);

		    // Personaliza a mensagem do resultado de acordo com a classificação do IMC
		    if (imc < 18.5) {
		    	float varWeight = ((18.5f * (height * height)) - weight);
		        message += String.format("kg/m², por isso você deveria ganhar no mínimo " + (int)  Math.round(varWeight) + "kg.");
		        resultImcTable = 1;
		    } else if (imc >= 24.9) {
		        float varWeight = (weight - (24.9f * (height * height)));
		        message += "kg/m², por isso você deveria emagrecer no mínimo " + (int) Math.round(varWeight) + "kg.";
		        if (imc <= 30) {
					resultImcTable = 3;
		        } else {
					resultImcTable = 4;
		        }
		    } else {
		        message += "kg/m² e você está dentro do peso adequado.";
				resultImcTable = 2;
		    }
		    
		    return message;
		}
		
		public int getIMCResultTable() {
			return resultImcTable;
		}
	%>

	<div class="container">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title"><%= isValidForm(request) ? "Seu resultado" : "Não foi possível calcular seu IMC" %></h5>
				<p class="card-text"><%= isValidForm(request) ? getIMCResult(request) : "Altura e (ou) peso inválidos!<br>Preencha sua altura (em metros) entre 0m e 3m e seu peso (em kilogramas) entre 0kg e 300kg." %></p>

				
				<div class="container-table" style="<%= isValidForm(request) ? "" : "display: none;" %>">
		          <table class="table">
		            <thead class="thead-light">
		              <tr>
		                <th scope="col">Classificação</th>
		                <th scope="col">IMC</th>
		              </tr>
		            </thead>
		            <tbody>
		              <tr class="<%= getIMCResultTable() == 1 ? "table-warning" : "" %>">
		                <td>Magreza</td>
		                <td>
		                  < 18.5</td>
		              </tr>
		              <tr class="<%= getIMCResultTable() == 2 ? "table-success" : "" %>">
		                <td>Normal</td>
		                <td>18.5 a 24.9</td>
		              </tr>
		              <tr class="<%= getIMCResultTable() == 3 ? "table-warning" : "" %>">
		                <td>Sobrepeso</td>
		                <td>24.9 a 30</td>
		              </tr>
		              <tr class="<%= getIMCResultTable() == 4 ? "table-danger" : "" %>">
		                <td>Obesidade</td>
		                <td>> 30</td>
		              </tr>
		            </tbody>
		          </table>
		        </div>

				<a href="index.jsp"><button type="button" class="btn btn-primary">Voltar</button></a>
			</div>
		</div>
	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>

</html>