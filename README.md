# Calculadora IMC com JSP

Projeto de Calculadora IMC utilizando HTML, CSS, Bootstrap e JSP.
Programação para Web, 2018/1 - Sistemas da Informação, UFG

## Como Utilizar

O projeto é bem simples, basta baixar o arquivo `calculadora-imc-jsp.war` e dar deploy em seu servidor java.

## Exemplo

Em breve

## Autores

* **Bruno Braz Silveira** - <a href="https://brunobraz.io">brunobraz.io</a>
